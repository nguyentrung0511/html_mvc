<div class="widget has-border">
    <div class="sidebar-product-size mb-30">
        <h4 class="widget-title">Product Size</h4>
        <div class="shop-size-list">
            <ul>
                <li><a href="#">S</a></li>
                <li><a href="#">M</a></li>
                <li><a href="#">L</a></li>
                <li><a href="#">XL</a></li>
                <li><a href="#">XXL</a></li>
            </ul>
        </div>
    </div>
    <div class="sidebar-product-color">
        <h4 class="widget-title">Color</h4>
        <div class="shop-color-list">
            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>
</div>
<div class="widget">
    <h4 class="widget-title">Top Items</h4>
    <div class="sidebar-product-list">
        <ul>
            <li>
                <div class="sidebar-product-thumb">
                    <a href="#"><img src="public/layout/adara/img/product/sidebar_product01.jpg" alt=""></a>
                </div>
                <div class="sidebar-product-content">
                    <div class="rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                    <h5><a href="#">Woman T-shirt</a></h5>
                    <span>$ 39.00</span>
                </div>
            </li>
            <li>
                <div class="sidebar-product-thumb">
                    <a href="#"><img src="public/layout/adara/img/product/sidebar_product02.jpg" alt=""></a>
                </div>
                <div class="sidebar-product-content">
                    <div class="rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                    <h5><a href="#">Slim Fit Cotton</a></h5>
                    <span>$ 39.00</span>
                </div>
            </li>
            <li>
                <div class="sidebar-product-thumb">
                    <a href="#"><img src="public/layout/adara/img/product/sidebar_product03.jpg" alt=""></a>
                </div>
                <div class="sidebar-product-content">
                    <div class="rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                    <h5><a href="#">Fashion T-shirt</a></h5>
                    <span>$ 39.00</span>
                </div>
            </li>
        </ul>
    </div>
</div>