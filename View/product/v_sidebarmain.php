<div class="widget">
    <h4 class="widget-title">Price Filter</h4>
    <div class="price_filter">
        <div id="slider-range"></div>
        <div class="price_slider_amount">
            <span>Price :</span>
            <input type="text" id="amount" name="price" placeholder="Add Your Price" />
        </div>
    </div>
</div>
<div class="widget">
    <h4 class="widget-title">Product Brand</h4>
    <div class="sidebar-brand-list">
        <ul>
            <li><a href="#">New Arrivals <i class="fas fa-angle-double-right"></i></a></li>
            <li><a href="#">Clothing & Accessories <i class="fas fa-angle-double-right"></i></a></li>
            <li><a href="#">Winter Jacket <i class="fas fa-angle-double-right"></i></a></li>
            <li><a href="#">Baby Clothing <i class="fas fa-angle-double-right"></i></a></li>
        </ul>
    </div>
</div>
