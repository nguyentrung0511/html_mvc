<!-- main-area -->
<main>

    <!-- slider-area -->
    <?php  include ("v_slider-area.php")?>
    <!-- slider-area-end -->

    <!-- category-banner -->
   <?php include ("v_category-banner.php") ?>
    <!-- category-banner-end -->

    <!-- promo-services -->
    <?php include ("v_promo-services.php")?>
    <!-- promo-services-end -->

    <!-- features-product-area -->
   <?php include ("v_features-product-area.php")?>
    <!-- features-product-area-end -->

    <!-- discount-area -->
    <?php include ("v_discount-area.php")?>
    <!-- discount-area-end -->

    <!-- trending-product-area -->
    <?php include ("v_trending-product-area.php")?>
    <!-- trending-product-area-end -->

    <!-- newsletter-area -->
    <?php include ("v_newsletter-area.php")?>
    <!-- newsletter-area-end -->

    <!-- brand-area -->
   <?php include ("v_brand-area.php")?>
    <!-- brand-area-end -->

    <!-- instagram-post-area -->
  <?php include ("v_instagram-post-area.php")?>
    <!-- instagram-post-area-end -->

</main>
<!-- main-area-end -->
