<section class="newsletter-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="newsletter-bg" data-background="public/layout/adara/img/bg/newsletter_bg.jpg">
                    <div class="newsletter-title mb-65">
                        <h2 class="title">NEWSLETTER!</h2>
                        <h6 class="sub-title">NEWSLETTER AND GET DISCOUNT 25% OFF</h6>
                    </div>
                    <form action="#" class="newsletter-form">
                        <input type="email" placeholder="Your email address...">
                        <button><span>Subscribe</span> <i class="far fa-arrow-alt-circle-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
