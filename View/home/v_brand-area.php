<div class="brand-area pt-100 pb-100">
    <div class="container">
        <div class="row brand-active">
            <div class="col">
                <div class="brand-item">
                    <a href="#"><img src="public/layout/adara/img/brand/brand_item01.png" alt=""></a>
                </div>
            </div>
            <div class="col">
                <div class="brand-item">
                    <a href="#"><img src="public/layout/adara/img/brand/brand_item02.png" alt=""></a>
                </div>
            </div>
            <div class="col">
                <div class="brand-item">
                    <a href="#"><img src="public/layout/adara/img/brand/brand_item03.png" alt=""></a>
                </div>
            </div>
            <div class="col">
                <div class="brand-item">
                    <a href="#"><img src="public/layout/adara/img/brand/brand_item04.png" alt=""></a>
                </div>
            </div>
            <div class="col">
                <div class="brand-item">
                    <a href="#"><img src="public/layout/adara/img/brand/brand_item05.png" alt=""></a>
                </div>
            </div>
            <div class="col">
                <div class="brand-item">
                    <a href="#"><img src="public/layout/adara/img/brand/brand_item06.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
