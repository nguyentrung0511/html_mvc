<section class="promo-services pt-50 pb-25">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-8">
                <div class="promo-services-item mb-40">
                    <div class="icon"><img src="public/layout/adara/img/icon/promo_icon01.png" alt=""></div>
                    <div class="content">
                        <h6>payment & delivery</h6>
                        <p>Delivered, when you receive arrives</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-8">
                <div class="promo-services-item mb-40">
                    <div class="icon"><img src="public/layout/adara/img/icon/promo_icon02.png" alt=""></div>
                    <div class="content">
                        <h6>Return Product</h6>
                        <p>Retail, a Product Return Process</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-8">
                <div class="promo-services-item mb-40">
                    <div class="icon"><img src="public/layout/adara/img/icon/promo_icon03.png" alt=""></div>
                    <div class="content">
                        <h6>money back guarantee</h6>
                        <p>Options Including 24/7</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-8">
                <div class="promo-services-item mb-40">
                    <div class="icon"><img src="public/layout/adara/img/icon/promo_icon04.png" alt=""></div>
                    <div class="content">
                        <h6>Quality support</h6>
                        <p>Support Options Including 24/7</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
