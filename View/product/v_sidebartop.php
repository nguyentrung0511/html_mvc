<div class="widget side-search-bar">
    <form action="#">
        <input type="text">
        <button><i class="flaticon-search"></i></button>
    </form>
</div>
<div class="widget">
    <h4 class="widget-title">Product Categories</h4>
    <div class="shop-cat-list">
        <ul>
            <li><a href="#">Accessories</a><span>(6)</span></li>
            <li><a href="#">Computer</a><span>(4)</span></li>
            <li><a href="#">Covid-19</a><span>(2)</span></li>
            <li><a href="#">Electronics</a><span>(6)</span></li>
            <li><a href="#">Frame Sunglasses</a><span>(12)</span></li>
            <li><a href="#">Furniture</a><span>(7)</span></li>
            <li><a href="#">Genuine Leather</a><span>(9)</span></li>
        </ul>
    </div>
</div>
