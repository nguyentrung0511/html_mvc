<!-- footer-area -->
<footer class="dark-bg pt-55 pb-80">
    <div class="container">
        <div class="footer-top-wrap">
            <div class="row">
                <div class="col-12">
                    <div class="footer-logo">
                        <a href="index.html"><img src="img/logo/w_logo.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle-wrap">
            <div class="row">
                <div class="col-12">
                    <div class="footer-link-wrap">
                        <nav class="menu-links">
                            <ul>
                                <li><a href="about-us.html">About us</a></li>
                                <li><a href="shop-sidebar.html">Store</a></li>
                                <li><a href="#">Locations</a></li>
                                <li><a href="contact.html">Contact</a></li>
                                <li><a href="contact.html">Support</a></li>
                                <li><a href="#">Policy</a></li>
                                <li><a href="#">Faqs</a></li>
                            </ul>
                        </nav>
                        <div class="footer-social">
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="copyright-text">
                        <p>&copy; 2021 <a href="index.html">adara</a>. All Rights Reserved | Ph (+09) 456 457869</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="pay-method-img">
                        <img src="img/images/payment_method_img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area-end -->





<!-- JS here -->
<script src="public/layout/adara/js/vendor/jquery-3.5.0.min.js"></script>
<script src="public/layout/adara/js/popper.min.js"></script>
<script src="public/layout/adara/js/bootstrap.min.js"></script>
<script src="public/layout/adara/js/isotope.pkgd.min.js"></script>
<script src="public/layout/adara/js/imagesloaded.pkgd.min.js"></script>
<script src="public/layout/adara/js/jquery.magnific-popup.min.js"></script>
<script src="public/layout/adara/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="public/layout/adara/js/bootstrap-datepicker.min.js"></script>
<script src="public/layout/adara/js/jquery.nice-select.min.js"></script>
<script src="public/layout/adara/js/jquery.countdown.min.js"></script>
<script src="public/layout/adara/js/swiper-bundle.min.js"></script>
<script src="public/layout/adara/js/jarallax.min.js"></script>
<script src="public/layout/adara/js/slick.min.js"></script>
<script src="public/layout/adara/js/wow.min.js"></script>
<script src="public/layout/adara/js/nav-tool.js"></script>
<script src="public/layout/adara/js/plugins.js"></script>
<script src="public/layout/adara/js/main.js"></script>
</body>

<!-- Mirrored from themepixer.com/demo/html/adara/adara/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 07 May 2021 08:04:49 GMT -->
</html>

