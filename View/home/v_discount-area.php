<section class="discount-area discount-bg jarallax parallax-img" data-background="public/layout/adara/img/bg/discount_bg.jpg">
    <div class="container">
        <div class="row justify-content-center justify-content-lg-start">
            <div class="col-lg-6 col-md-10">
                <div class="discount-content text-center">
                    <div class="icon mb-15"><img src="public/layout/adara/img/icon/discount_icon.png" alt=""></div>
                    <span>STOCK IS LIMITED</span>
                    <h2>Winter Collection End of Season Sale upto 30%</h2>
                    <a href="shop-sidebar.html" class="btn">shop now</a>
                </div>
            </div>
        </div>
    </div>
</section>
